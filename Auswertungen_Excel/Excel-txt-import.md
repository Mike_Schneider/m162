![TBZ Logo](../x_gitressourcen/tbz_logo.png)

# Excel TXT-Datei Import

Bei den Dateitypen nach allen Dateien filtern oder nach dem spezifischen Datei-Typ, den sie importieren möchten.

![Schritt1](./Excel-txt-import/excel_import_txt_1.png)

---

Achten sie auf die Vorschau. Wenn die Umlaute oder andere Sonderzeichen nicht korrekt dargestellt werden, dann ist ihre Datei UTF-8 encodiert und sie müssen das Encoding entsprechend auswählen. Suchen sie im Dropdown nach UTF-8.

![Schritt2](./Excel-txt-import/excel_import_txt_2.png)

---

Legen sie hier fest, welches Zeichen ihr Separator ist. Sie können auch ein belibiges Zeichen angeben. Wichtig ist ebenfalls, dass sie Auswählen, welches Zeichen ihre Zeichenketten umgibt. Dies ist entweder das Double-Quote (doppeltes Anführungszeichen) oder das Single-Quote (einfaches Anführungszeichen).

![Schritt3](./Excel-txt-import/excel_import_txt_3.png)

---

&copy;TBZ, 2021, Modul: m162