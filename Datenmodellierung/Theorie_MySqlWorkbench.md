![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# MySql Workbench

[TOC]

## Download

[Laden sie zuerst MySql Workbench herunter](https://dev.mysql.com/downloads/workbench/). Sie können den Online- oder Offline-Installer verwenden. Ich empfehle das Zweite. Sie müssen sich nicht einloggen, um die Applikation herunterzuladen.

Entweder sie folgen den Anweisungen unten oder verwenden den [direkten Link hier](https://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-community-8.0.27.1.msi).

![Download1](./x_gitressourcen/MySqlWorkbench/download1.png)
![Download2](./x_gitressourcen/MySqlWorkbench/download2.png)
![Download3](./x_gitressourcen/MySqlWorkbench/download3.png)

## Installation

Für den Unterricht dieses Moduls benötigen wir nur die Client-Tools, also keinen MySql-Server. Sie dürfen den gerne ebenfalls installieren, wobei ein Service installiert wird, der im Hintergrund läuft. Bei zukünftigen Module **kann es sein**, dass sie auch einen SQL-Server (nach-)installieren müssen.

![Install1](./x_gitressourcen/MySqlWorkbench/install1.png)

Am Schluss können sie einen MySql Server konfigurieren. Lassen sie dort einfach alle Felder frei.

## Krähenfuss Notation

MySql Workbench verwendet die Krähenfuss Notation (crow’s foot notation) um 1:m Beziehungen abzubilden. Da im physischen Modell nur dieser Beziehungs-Typ existiert, werden wir nicht im Detail auf die Unterschiede der Notation zwischen Zender und Krähenfuss eingehen. Die nachfolgenden Beispiele zeigen wie die Symbole verwendet werden.

Für Interessierte erklärt [dieser Link](https://vertabelo.com/blog/crow-s-foot-notation/) die Symbole.

## Interface

Wenn sie MySql Workbench starten, sehen sie das folgende Interface. Überhalb des Bereichs für ERM könnten sie sich mit einer Datenbank verbinden, remote oder lokal. Wir befassen uns aber hier mit ERMs.

![interface1](./x_gitressourcen/MySqlWorkbench/interface1.png)

Wenn sie ein neues ERM erstellen, finden sie das folgende Bild vor. MySql Workbench zeigt die Unterscheidung in ERM und ERD deutlich. Sie haben nun ein Modell erstellt (ERM) und können mehrere Diagramme (ERD) hinzufügen.

![interface2](./x_gitressourcen/MySqlWorkbench/interface2.png)

Nachdem sie ein neues Diagramm hinzugefügt haben, sehen sie auf der linken Seite Die Objekte, die bereits existieren. In diesem Fall natürlich noch keine. Sie können über die Symbole neue Tabellen anlegen. Klicken sie auf das entsprechende Symbol und dann klicken sie in den leeren Bereich auf der rechten Seite. Eine neue Tabelle wird erstellt.

Damit sie die Attribute bearbeiten können, müssen sie die Tabelle Doppelklicken oder via Rechtsklick -> Edit Table den unteren Bereich öffnen. Hier sehen sie die verschiedenen Einstellungen zu *PK*, *Not Null* und *Unique*. Ein *PK* ist automatisch *Unique*.

Die Eigenschaft *AI* bedeutet, dass für dieses Feld automatisch ein neuer Wert generiert wird. Eine Zahl wird automatisch bei neuen Einträgen hochgezählt.

![interface3](./x_gitressourcen/MySqlWorkbench/interface3.png)

Im folgenden Bild sehen sie eine 1:m Beziehung. **Sie müssen den FK nicht manuell erstellen**, MySql Workbench übernimmt das für sie. Erstellen sie die beiden Tabellen mit den *PK*s und den anderen Attributen, aber ohne *FK*s.

Über das Symbol in der Leiste, können sie die Verknüpfung erstellen und der *FK* wird automatisch eingetragen. Sie können das Feld natürlich anschliessend umbenennen.

Beachten sie die Farben und Symbole bei den Tabellen:

- gelber Schlüssel: Primärschlüssel
- blauer Diamant: Einfaches Attribut
- roter Diamant: Fremdschlüssel
- roter Schlüssel: Primärschlüssen, der auch Fremdschlüssel ist. Ein Beispiel sehen sie später.

![interface4](./x_gitressourcen/MySqlWorkbench/interface4.png)

Auch m:m Beziehungen kann MySql Workbench automatisch erstellen. sei müssen die Transformationstabelle nicht mal selbst erstellen.

![interface5](./x_gitressourcen/MySqlWorkbench/interface5.png)

Verwenden sie das korrekte Symbol in der Leiste für die m:m Beziehung und klicken sie die entsprechenden Tabellen an.

![interface6](./x_gitressourcen/MySqlWorkbench/interface6.png)

Es kommt oft die Frage, ob man die Beziehung mit der gestrichelten oder ausgezogenen Linie verwenden muss. Grundsätzlich verwenden sie die gestrichelte, ausser für m:m Beziehungen (sie haben hier nur eine Option).

Der Unterschied liegt darin wie der *FK* hinzugefügt wird. Bei der ausgezogenen Linie wird der *FK* auch Teil des *PK*s. Dies ist typischerweise der Fall bei m:m Beziehungen, aber nicht erwünscht bei normalen 1:m Beziehungen. Schauen sie sich folgendes Beispiel an.

Mehr dazu finden sie auf der [Seite von MySql Workbench](https://dev.mysql.com/doc/workbench/en/wb-relationship-tools.html).

![interface6](./x_gitressourcen/MySqlWorkbench/interface7.png)

---

&copy;TBZ, 2021, Modul: m162
