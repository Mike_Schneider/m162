![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Cineville": Kundenauftrag

Sie können sich mit einer „vagen“ Kundenbeschreibung auseinander setzen. Sie schaffen es aus dieser eine zukunftsorientierte Datenbank in der 3. Normalform zu entwickeln.

Dabei nehmen sie die Rolle des DB-Architekten ein und erarbeiten zusammen mit dem Kunden die Details und erstellen eine Dokumentation.

#### Aufgabe

Das Kino CineVille will seine Filme, Vorführungen, Termine, Ticketverkauf, Vorverkauf, Kunden und Kinos mit einer neuen Datenbank verwalten. Folgende Informationen werden erfasst:

1. Liste von Filmen, jeweils mit ihrer Länge und Sprache
2. die einzelnen Tage, die der Film läuft, mit den entsprechenden Uhrzeiten
3. dem Preis, dem Mindestalter, dem Kino, in dem der Film läuft
4. Kinosäle mit der Anzahl Plätze im Saal und der Wechselzeit (das ist die Zeit, die benötigt wird, das Kino zu räumen und mit dem Publikum des folgenden Films zu füllen).
5. Der Eintrittspreis ist für eine bestimmte Vorstellung für alle Plätze gleich.
6. Stammkunden mit Adresse und Telefonnummer, teilweise E-Mail Adresse.

![cineville](x_gitressourcen/cineville1.png)

**Tasks**:

- Erkennen sie die bestehenden Anforderungen und erstellen sie das ERM
- Denken sie für ihren Kunden weiter und erweitern sie das Modell, z.B. Parkplätze
- Die Aufteilung der Attribute in mehrere Tabellen muss dabei der 3. Normalform entsprechen.

#### Zeit und Form

- 3-4 Lektionen
- Individuell

---

&copy;TBZ, 2021, Modul: m162
