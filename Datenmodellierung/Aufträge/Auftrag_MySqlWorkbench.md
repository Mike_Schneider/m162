![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "MySql Workbench"

In dieser Übung werden sie die Applikation MySql Workbench installieren und sich damit vertraut machen. Wir werden diese Applikation nun regelmässig verwenden, um physische ERDs zu erstellen. 

#### Aufgabe

Lesen sie sich durch die [Theorie über MySql Workbench](../Theorie_MySqlWorkbench.md) und kopieren die vorgezeigten Schritte.

**Tasks**:

- Installation MySql Workbench
- Tabellen erstellen
- 1:m und m:m Beziehungen erstellen

#### Zeit und Form

- 45 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162