![TBZ Logo](../x_gitressourcen/tbz_logo.png)

---

# Dateitypen und Strukturierungsgrad

[TOC]

Folgend nur ein paar wichtige Dateitypen und deren Strukturierungsgrad.

## Text

Text ist eine **„Anhäufung“ von zusammengehörenden Zeichen** (ASCII, UTF-8, etc) die einen **Sachverhalt beschreiben**. Diese Form ist besonders gut geeignet für Volltextsuche. Text-Strukturen sind im Sinne der Strukturierung sehr schwach strukturiert. Da eine Darstellung in Textdateien bei einer Suche nicht hilft. Allerdings sind diese „Strukturen“ ganz leicht zu bearbeiten.

**Typische Dateierweiterungen**: TXT, JSON; XML, CSV, HTML

## Grafik

Eine grafische Darstellung ist „nur“ eine **Momentaufnahme**. Die in der Grafik enthaltene Information ist maschinell nicht (direkt) lesbar, demnach ist sie unstrukturiert. Die Daten aber, d.h. die elektronische Speicherung, sind nat. strukturiert (z.B. Pixel-Raster) --> Bildformate. 
Dazu gibt es zwei grundlegend verschiedene Möglichkeiten: 

- die Punktförmige Speicherung (**Pixelgrafik**) oder 
- die berechnete Darstellung (**Vektorgrafik**). Hier wird jeder Punkt berechnet.

 Unterschiede bei den Bildformaten: https://www.labnol.org/software/tutorials/jpeg-vs-png-image-quality-or-bandwidth/5385/

**Typische Dateierweiterungen**: PNG, JPG, GIF, SVG

## Video / Sound

Sowohl Video und Sound sind **analoge Signale, die digitalisiert werden**. Auch hier sprechen wir von KEINER Struktur der Information.

Die Daten aber, d.h. die elektronische Speicherung, sind nat. strukturiert (Samples).

**Typische Dateierweiterungen**: WAV, MP3, MP4, AVI, MPEG, MKV

---

&copy;TBZ, 2022, Modul: m162