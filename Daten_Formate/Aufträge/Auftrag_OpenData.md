![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Open Data

 Verwenden sie eine Open Data Quelle (z.B. der Stadt Zürich, BFS oder andere öffentliche Quellen) und erstellen sie ein Dokument in dem sie folgende Themen abdecken:

- Zeigen sie wie man an ihre spezifisch ausgewählten Daten kommt. Beschreiben sie die Schritte (z.B. Registrierung, Brauchen sie API-Key oder nicht, etc). Sie können auch mit Screenshots arbeiten.
- Erklären sie die Felder und Inhalte der Daten (mindestens die wichtigsten)
- Beschreiben sie wie die Daten nützlich sind und welchen Anwendungszweck man damit verfolgen könnte.
- Erstellen sie ein Diagramm aus den Daten. 


#### Zeit und Form

30 Minuten

2-3er Gruppen

---

&copy;TBZ, 2022, Modul: m162